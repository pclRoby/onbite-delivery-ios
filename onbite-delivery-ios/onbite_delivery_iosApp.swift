//
//  onbite_delivery_iosApp.swift
//  onbite-delivery-ios
//
//  Created by Robica on 12/01/2021.
//

import SwiftUI

@main
struct onbite_delivery_iosApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
